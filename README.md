# Device Tree for Samsung Galaxy A20 (SM-A205G)
Device Tree Made by Enzo and edited by Renatoh Ribeiro
## Specs

|        Component        |          Specification            |
| :---------------------- | :-------------------------------- |
| Chipset                 | Exynos 7884                       |
| Memory                  | 3 GB                              |
| Storage                 | 32GB                              |
| Battery                 | 4000 mAh (non-removable)          |
| Dimensions              | 158.4 x 74.7 x 7.8 mm             |
| Display                 | 720 x 1560 pixels, 19.5:9, 268PPI |
| Release Date            | 2019 April                        |

## Device Picture

![Galaxy A20](https://fdn2.gsmarena.com/vv/bigpic/samsung-galaxy-a20.jpg "Galaxy A20")